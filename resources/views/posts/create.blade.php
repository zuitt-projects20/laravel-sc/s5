@extends('layouts.app')

@section('content')
	<h1>Create Post</h1>
	<form action="{{ action('PostController@store') }}" method="POST">
		@csrf
		<div class="form-group">
			<label for="title-input">Title</label>
			<input id="title-input" type="text" name="title" class="form-control" placeholder="Title">
		</div>

		<div class="form-group">
			<label for="body-input">Body</label>
			<textarea id="body-input" name="body" class="form-control" placeholder="Body" rows="5"></textarea>			
		</div>

		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
@endsection

{{-- // 

ACTIVITY:
1. Edit navbar.blade.php to show a dropdown link under "Blog"
	-Inside the dropdown will be the following links:
		Posts: Leading to posts/index.blade.php
		Create: Leading to posts/create.blade.php

2. Change our index.blade.php to show the newest post at the top of the list

When done, push your work and paste your -complete repo link- to Boodle under session 3
	*Please also do the same for session 2

--}}