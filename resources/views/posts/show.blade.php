@extends('layouts.app')

@section('content')
	<h1>{{$post->title}}</h1>
	<small>Written on {{$post->created_at}}</small>

	<div class="my-4">
		{{$post->body}}
	</div>

	@if(!Auth::guest())
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#commentModal">
		  Post comment
		</button>
	@endif

	@if(count ($post->comments) > 0)
		<h5 class="mt-5">Comments:</h5>
		<div class="card">
			<ul class="list-group list-group-flush">
				@foreach($post->comments as $comment)
					<li class="list-group-item">
						<p class="text-center">{{$comment->content}}</p>
						<p class="text-right">posted by: {{$comment->user->name}}</p>
						<p class="text-right">posted on: {{$comment->created_at}}</p>
					</li>
				@endforeach
			</ul>
		</div>
	@endif

	<!-- Modal -->
	<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Post a comment</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="/posts/{{$post->id}}/comment" method="POST">
	        	@csrf
	        	<div class="form-group">
	        		<label for="content">Content:</label>
	        		<textarea class="form-control" name="content" id="content" rows="4"></textarea>
	        	</div>
	        	<button type="submit" class="btn btn-primary">Submit</button>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection